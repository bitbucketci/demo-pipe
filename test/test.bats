#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/demo-pipe"}

  echo "Building image..."
  run docker build -t ${DOCKER_IMAGE} .
}

teardown() {
    echo "Teardown happens after each test."
}

@test "should echo 'Hello world baz' to stdout" {
    run docker run \
        -e NAME="baz" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}
    echo $status
    echo $output
    [[ "$status" == "0" ]]
    [[ "$output" =~ "Hello world baz" ]]
}

@test "should echo 'Hello world baz' to the file task.output" {
    run docker run \
        -e NAME="baz" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}
    [[ "$status" == "0" ]]
    [[ "$(cat pipe.output)" == "Hello world baz" ]]
}

@test "should should greeting to be overridden" {
    run docker run \
        -e NAME="baz" \
        -e GREETING="Good morning" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}
    [[ "$status" == "0" ]]
    [[ "$(cat pipe.output)" == "Good morning baz" ]]
}


teardown() {
    echo "Teardown happens after each test."
}
