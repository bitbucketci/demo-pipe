# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.1

- patch: Task standardisation and cleanup.

## 0.2.0

- minor: Update pipe to latest naming and documentation conventions.

## 0.1.1

- patch: Changes in tests

## 0.1.0

- minor: Initial release

